### Задания по вычислительной практике
### Булавская Камилла, 2 курс, 8 группа

#### Индивидуальные задания по алгоритмитизации:
* ##### Инд. зад. №1 [вариант 1](https://bitbucket.org/feelingbllue/mmf_2020_2_course/src/master/TaskAlgoritm/)
* ##### Инд. зад. №2 [вариант 3](https://bitbucket.org/feelingbllue/mmf_2020_2_course/src/master/TaskAlgoritm2/)
* ##### Инд. зад. №3 [вариант 6](https://bitbucket.org/feelingbllue/mmf_2020_2_course/src/master/TaskAlgoritm3/)

#### Групповое задание:
##### Задания по созданию [веб-страницы](https://bitbucket.org/feelingbllue/mmf_2020_2_course/src/master/webpage%20creation/) и [визуализации результатов](https://bitbucket.org/feelingbllue/mmf_2020_2_course/src/master/webpage%20creation/). [Результат работы](https://bookingbyegor.herokuapp.com/) группового задания.

