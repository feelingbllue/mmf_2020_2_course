import solve

def solve1():
    ans = []

    print("solveOne.")
    st = set()

    flag = True

    print("Words: ")

    for i in range(0, 10):
        a = solve.generatePass()

        ans.append(a)

        print(a)        

        if (a in st):
            flag = False
        else:
            st.add(a)
        
    if (flag):
        print("\nAll words are unique")
    else:
        print("\nThere are words twins")

    return ans

def solve2(words):
    print("\nsolveTwo.\n")


    for q in range(0, len(words)):
        print("\n", words[q])

        for i in range(1, len(words[q])):
            if('A' <= words[q][i] <= 'Z'):
                ind = i
                break
        print(" first word: ")

        fWord = words[q][:ind]
        # print(fWord)

        for i in range(0, len(solve.data)):
            if (fWord in solve.data[i]):
                print(solve.data[i])

        print(" second word: ")

        fWord = words[q][ind:]
        # print(fWord)

        for i in range(0, len(solve.data)):
            if (fWord in solve.data[i]):
                print(solve.data[i])
    # pass

        
words = solve1() 
solve2(words)
