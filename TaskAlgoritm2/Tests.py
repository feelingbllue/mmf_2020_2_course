import Test
from solve import seq_level, find

Test.assert_equals(seq_level([1, 2, 3, 4, 5]), "Linear")
Test.assert_equals(seq_level([2, 1, 0, -1, -2]), "Linear")
Test.assert_equals(seq_level([3, 6, 10, 15, 21]), "Quadratic")
Test.assert_equals(seq_level([4, 9, 16, 25, 36]), "Quadratic")
Test.assert_equals(seq_level([7, 17, 31, 49, 71]), "Quadratic")
Test.assert_equals(seq_level([2, 10, 26, 50, 82]), "Quadratic")
Test.assert_equals(seq_level([-6, -4, 10, 42, 98, 184]), "Cubic")
Test.assert_equals(seq_level([17, 59, 143, 287, 509, 827]), "Cubic")