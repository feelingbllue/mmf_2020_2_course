def find(arr):
    newArr = []
    for i in range(1, len(arr)):
        newArr.append(arr[i] - arr[i - 1])
    
    flag = True

    for i in range(1, len(newArr)):
        if newArr[i] != newArr[i - 1]:
            flag = False

    return [flag, newArr]

def seq_level(arr):
    firstTry = find(arr)
    if (firstTry[0]):
        return "Linear"

    secondTry = find(firstTry[1])
    if (secondTry[0]):
        return "Quadratic"
    
    thirdTry = find(secondTry[1])
    if (thirdTry[0]):
        return "Cubic"
    else:
        return "ERROR"
    


