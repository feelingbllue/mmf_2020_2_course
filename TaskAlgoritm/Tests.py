import Test
from solve import returnUnique

Test.assert_equals(returnUnique([1, 9, 8, 8, 7, 6, 1, 6]),                [9, 7])
Test.assert_equals(returnUnique([5, 5, 2, 4, 4, 4, 9, 9, 9, 1]),          [2, 1])
Test.assert_equals(returnUnique([9, 5, 6, 8, 7, 7, 1, 1, 1, 1, 1, 9, 8]), [5, 6])
Test.assert_equals(returnUnique([9, 6, 3, 2, 6, 4, 9, 6, 3, 2, 0]),       [4, 0])
Test.assert_equals(returnUnique([1, 2, 3, 4, 6, 7, 1, 2, 3, 4]),          [6, 7])
Test.assert_equals(returnUnique([5, 6, 6, 5, 6, 9, 9, 1, 0]),             [5, 0])
Test.assert_equals(returnUnique([2, 3, 5, 1, 2, 3, 4, 4, 5, 6]),          [5, 6])
Test.assert_equals(returnUnique([11, 11, 12, 13, 14, 13]),                [14, 13])
Test.assert_equals(returnUnique([2, 3]),                                  [3, 2])
Test.assert_equals(returnUnique([3, 4, 5, 5, 4, 6]),                      [4, 6])